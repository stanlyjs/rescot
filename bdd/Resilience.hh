/* Code for RESCOT */
/*
 * Resilience.hh
 *
 *  created on: 01.10.2019
 *      author: Stanly Samuel
 *      
 */

#include "SymbolicModel.hh"
#include "SymbolicSet.hh"
#include "FixedPoint.hh"
#include "TicToc.hh"
#include <climits>

#include<map>

using namespace std;
namespace rescot {
/*
 * class: Resilience
 *
 * It provides functions riskUpdate and disturbanceUpdate for computing resiliences of vertices as per the paper Resilient Abstraction Based Controller Design by Samuel et. al.
 *
 */

/* 
The input to the class are two Symbolic Models abstraction_d1 and abstraction_d2 (abstraction_d2 is assumed to be a superset of abstraction_d1)
since we give as input the disturbance values to SCOTS appropriately and SCOTS computes the superset using the growth bound). 
This class is used to compute resilience values for each node. For this we need the disturbance transition relation abstraction_d which we
get by taking the difference of abstraction_d2 and abstraction_d1. 
In the computation process, we keep cutting off the state space of abstraction_d1 (and abstraction_d, need to think) but
abstraction_d2 is kept untouched since we need it in the end for the riggged update.
*/

/*
We also assume that the state space and input space of the abstractions are all the same
*/

/* TODOS for sanity check and total completeness:
1) C++: Use initializer lists so that abstraction_d, ss, is, sspost can be initlialized within the class. Then remove the initializations from functions and don't pass abstraction_d in dist update.
Also, ss,is,sspost for this entire setup can be used directlyß. Can remove the function from Symbolic model?
5) Pass a general game. Then c0, c1, c2, c3 may not be needed as local variables here.
13) Add improve strategies function
14) Parity computations: First(r0), Last(rigged) and intermediate risk updates. How to optimize? Maybe can't. Fundamental bottleneck with naive algorithm. Disturbance updates take negligible time.
*/

    class Resilience{

        private:
            scots::SymbolicModel* abstraction_d1;
            scots::SymbolicModel* abstraction_d2;
            vector<scots::SymbolicSet*> ranking_function;
            scots::SymbolicSet* omegaResStates;
            scots::SymbolicSet* omega1ResStates;
            scots::SymbolicSet* controller_resilient;
            scots::SymbolicSet* controller_intermediate;
            // scots::SymbolicSet ss;
            // scots::SymbolicSet is;
            // scots::SymbolicSet sspost;
            scots::SymbolicSet* c0;
            scots::SymbolicSet* c1;
            scots::SymbolicSet* c2;
            scots::SymbolicSet* c3;

        public:
            Resilience(scots::SymbolicModel& abstraction_d1, scots::SymbolicModel& abstraction_d2, scots::SymbolicSet& c0, scots::SymbolicSet& c1, scots::SymbolicSet& c2, scots::SymbolicSet& c3)
            {
                //Want abstraction_d1, abstraction_d2 and W1 to point to the same object hence using pointers and not copying.
                this->abstraction_d1 = &abstraction_d1;
                this->abstraction_d2 = &abstraction_d2;
                // this->ss.setSymbolicSet(abstraction_d1.getStateSpace().getSymbolicSet());
                // this->is = abstraction_d1.getInputSpace();
                // this->sspost = abstraction_d1.getStateSpacePost();
                this->c0 = &c0;
                this->c1 = &c1;
                this->c2 = &c2;
                this->c3 = &c3;
                //Dummy update for now. Use initializer lists later. Want them to be local variables and not heap variables.
                this->omega1ResStates = &c0;
                this->omegaResStates = &c1;
            }
            void calculateResiliences()
            {
                TicToc tt;
                scots::SymbolicSet ss(abstraction_d1->getStateSpace());
                scots::SymbolicSet is(abstraction_d1->getInputSpace());
                scots::SymbolicSet sspost(abstraction_d1->getStateSpacePost());

                // Take the difference of the two transition relations to get the disturbance relation.
                
                scots::SymbolicModel abstraction_d(&ss,&is,&sspost);  
                abstraction_d.setTransitionRelation(abstraction_d2->getTransitionRelation().getSymbolicSet() & !abstraction_d1->getTransitionRelation().getSymbolicSet());
                scots::SymbolicSet a_d = abstraction_d.getTransitionRelation();
                a_d.writeToFile("abstraction_d.bdd");
                std::cout << std::endl << "Total space details:" << std::endl;
                a_d.printInfo(1);   

                //--1---------------------------------Solve initial game-----------------------------------------

                cout<<"\n";
                cout<<"\n";
                cout<<"----------------------------------SOLVING INITIAL GAME: (FOR RESILIENCE: 0). ----------------------------------";
                cout<<"\n";
                cout<<"\n";
                cout<<"\n";
                //First compute the game (in this case parity) to get the initial winning region
                scots::FixedPoint fp(abstraction_d1);
                tt.tic();
                BDD C = fp.parity((*c0).getSymbolicSet(), (*c1).getSymbolicSet(), (*c2).getSymbolicSet(), (*c3).getSymbolicSet()); //Controller
                std::cout<<"Initial game ";
                tt.toc();
                BDD WC = C.ExistAbstract((is).getCube()); //Winning region

                scots::SymbolicSet controller(ss,is);
                controller.setSymbolicSet(C);
                controller.writeToFile("controller.bdd");

                scots::SymbolicSet W0(ss);
                W0.setSymbolicSet(WC);
                W0.writeToFile("W0.bdd");

                scots::SymbolicSet adversary_controller(ss,is);
                adversary_controller.setSymbolicSet((!WC) & ss.getSymbolicSet()); //'& ss.getSymbolicSet()' is not needed. This is just (for ease of readability) to eliminate extra 'ghost' points (like 10->15 in this case) which appear in matlab output but on checking with isElement, it does not exist.
                adversary_controller.writeToFile("adversary_controller.bdd");

                scots::SymbolicSet* W1 = new scots::SymbolicSet(ss);
                W1->setSymbolicSet(adversary_controller.getSymbolicSet());
                W1->writeToFile("W1.bdd");

                //Mark W1 as resilience 0
                ranking_function.push_back(W1);
                ranking_function[0]->writeToFile("r0.bdd");

                //--2-----------------------------------Compute finite resiliences-----------------------------------------
                controller_resilient = new scots::SymbolicSet(abstraction_d2->getStateSpace(), abstraction_d2->getInputSpace());
                controller_resilient->setSymbolicSetZero();

                //We require a data structure to keep track of previous W0 (for future risk updates, to update resilient controller)
                controller_intermediate = new scots::SymbolicSet(controller);
                controller_intermediate->setSymbolicSet(controller.getSymbolicSet());

                int res_count = 1;
                while(disturbanceUpdate(abstraction_d, res_count))
                {                   
                    cout<<"\n";
                    cout<<"\n";
                    cout<<"----------------------------------SOLVING RISK UPDATE GAME FOR RESILIENCE:";
                    cout<<res_count;
                    cout<<" ----------------------------------";
                    cout<<"\n";
                    cout<<"\n";
                    cout<<"\n";
                    tt.tic();
                    riskUpdate();
                    std::cout<<"Risk update game for resilience: ";
                    cout<<res_count;
                    cout<<" ";
                    tt.toc();
                    res_count++;
                }

                cout<<"\n";
                cout<<"\n";
                cout<<"----------------------------------SOLVING RIGGED GAME: Finite resilience computation done. ----------------------------------";
                cout<<"\n";
                cout<<"\n";
                cout<<"\n";

                //--3---------------------------------Compute remaining resiliences (Omega and Omega+1)-----------------------------------------
                //Solve rigged game.
                //Winning region of rigged game for player 0 (W0ru) are the omega+1 resilient states.
                scots::FixedPoint fp_ru(abstraction_d2);
                BDD W0_ru = fp_ru.parity((*c0).getSymbolicSet(), (*c1).getSymbolicSet(), (*c2).getSymbolicSet(),(*c3).getSymbolicSet()); 
                BDD W0ru = W0_ru.ExistAbstract(is.getCube()); //{2:0, 3:0} w+1 states
                scots::SymbolicSet controller_rigged(ss,is);
                omega1ResStates->setSymbolicSet(W0ru);
                omega1ResStates->writeToFile("omega1ResStates.bdd");
                controller_rigged.setSymbolicSet(W0_ru);
                controller_rigged.writeToFile("controller_rigged.bdd");

                //Winning region of rigged game for player  (W1_ru) are the omega resilient states.
                scots::SymbolicSet W1_ru(ss);
                W1_ru.setSymbolicSet((!W0ru) & abstraction_d1->getStateSpace().getSymbolicSet());
                //w states should get their strategy from previous controller_intermediate which stores the latest winning region of P0 from the finite resilience computation.
                W1_ru.writeToFile("W1_rigged_update.bdd"); //{8,1} 
                omegaResStates->setSymbolicSet(W1_ru.getSymbolicSet());
                omegaResStates->writeToFile("omegaResStates.bdd");

                //Update resilient controller with omega resilient strategy: Previous controller is W0:Strategy (from last risk update stored in controller_intermediate) but for W1 states from rigged game stored in omegaResStates
                controller_resilient->setSymbolicSet(controller_resilient->getSymbolicSet() | (controller_intermediate->getSymbolicSet() &  omegaResStates->getSymbolicSet()));
                
                //Final: Update resilient controller with omega+1 resilient strategy: Just union with the strategy obtained from the last rigged game winning region viz. BDD W0_ru. No need of controller_intermediate anymore.
                controller_resilient->setSymbolicSet(controller_resilient->getSymbolicSet() | W0_ru);
                controller_resilient->writeToFile("controller_resilient.bdd");
            }

            bool disturbanceUpdate(scots::SymbolicModel& abstraction_d, int res_count)
            {
            TicToc tt;
            tt.tic();
            scots::FixedPoint fpd(&abstraction_d);
            scots::FixedPoint fp(abstraction_d1);
            
            //Definitions:
            //Frontier States: States updated by this disturbance update.
            //Non-Frontier States: States not upated by this disturbance update due to the presence of a non-spoiling input.
            //(A non spoiling input is an input that on a normal transition takes the system into states outside the states with already computed resilience and on a disturbance edge, does not take you into an already computed resilience state.)
            //Disturbance States: All states for which there exists a disturbance edge leading into an already computed resilience state.
            //Non-Frontier-Disturbance States: Non-Frontier States having some disturbance edge going into an already computed resilience state.
            

            //-------------------Disturbance update begins--------------------------
            //First we compute Non-Frontier states.
            BDD nonFrontierStateInputs =(fp.pre(!ranking_function[ranking_function.size()-1]->getSymbolicSet())  & !fpd.uPre(ranking_function[ranking_function.size()-1]->getSymbolicSet()));
            BDD nonFrontierStates = nonFrontierStateInputs.ExistAbstract(abstraction_d2->getInputSpace().getCube()); 
            //Non Frontier has set of states from which there exists a "nonspoiling" strategy to stay outside A. But this can be states with some other disturbance edge leading into A and outside A. 
            //So to see which one is actually the spoiling for the one's not updated by SPre (cos we need to remove them from abstraction), take uPre of A over abstraction_d and intersect those states to get the strategies to prune out.
            //Lastly, we compute Frontier States and update in ranking function. Frontier States = States which are not Non-Frontier States and not states with already computed resilience
            BDD frontierStates = (!nonFrontierStates) & !(ranking_function[ranking_function.size()-1]->getSymbolicSet()) & abstraction_d2->getStateSpace().getSymbolicSet();   
            scots::SymbolicSet* intermediate = new scots::SymbolicSet(*(ranking_function[ranking_function.size()-1]));
            intermediate->setSymbolicSet(frontierStates);
            //-------------------Disturbance udpate ends----------------------------


            //-------------------Strategy Pruning begins----------------------------
            //Strategy Pruning: Prune normal strategies from non-frontier states that has a disturbance edge leading into an already computed resilience state.
            BDD disturbanceStateInputs = fpd.uPre(ranking_function[ranking_function.size()-1]->getSymbolicSet()); //All states for which there exists a disturbance edge leading into an already computed resilience state.
            BDD strategiesToPrune = disturbanceStateInputs & nonFrontierStates; //Strategies to prune out of abstraction_d1. Non-spoiling strategies will never be a part of strategies to prune, be definition of not having disturbance edges leading into an already computed state.
            abstraction_d1->setTransitionRelation(abstraction_d1->getTransitionRelation().getSymbolicSet() & !strategiesToPrune); //Pruning
            // It suffices (and is necessary) to remove transitions from abstraction_d1. 
            // Doesn't matter if we remove from abstraction_d or abstraction_d2.
            //-------------------Strategy Pruning ends------------------------------

            //-------------Optimisation: Cutoff previous resilient states from all graphs------------------------

            //Initialize respective FixedPoint classes to cut off each transition relation.
            scots::FixedPoint fp_cut_d1(abstraction_d1);
            scots::FixedPoint fp_cut_d2(abstraction_d2); 
            scots::FixedPoint fp_cut_d(&abstraction_d);
            // Don't push if no new states are added => fixpoint reached
            if(intermediate->getSize() !=0)
            {
                //Update ranking function iff new resilient states discovered
                ranking_function.push_back(intermediate);

                //Prune both abstractions with previous resiliences.                   
                abstraction_d1->setTransitionRelation(fp_cut_d1.subGraph(ranking_function[ranking_function.size()-2]->getSymbolicSet()));
                abstraction_d2->setTransitionRelation(fp_cut_d2.subGraph(ranking_function[ranking_function.size()-2]->getSymbolicSet()));
                abstraction_d.setTransitionRelation(fp_cut_d.subGraph(ranking_function[ranking_function.size()-2]->getSymbolicSet()));
                std::cout<<"Disturbance update for resilience: ";
                cout<<res_count;
                cout<<" ";
                tt.toc();
                return true;
            }
            //Last disturbance update: Gave no new resilient states. Remove the last resilient states from the graph to be left with only omega and omega+1 states.
            
            //Prune both abstractions with finite resiliences.  
            abstraction_d1->setTransitionRelation(fp_cut_d1.subGraph(ranking_function[ranking_function.size()-1]->getSymbolicSet()));         
            abstraction_d2->setTransitionRelation(fp_cut_d2.subGraph(ranking_function[ranking_function.size()-1]->getSymbolicSet()));
            abstraction_d.setTransitionRelation(fp_cut_d.subGraph(ranking_function[ranking_function.size()-1]->getSymbolicSet()));
            std::cout<<"Disturbance update for resilience: ";
            cout<<res_count;
            cout<<" ";
            tt.toc();
            return false;
            }

            void riskUpdate()
            {

            scots::SymbolicSet ss(abstraction_d1->getStateSpace());
            scots::SymbolicSet is(abstraction_d1->getInputSpace());
            scots::SymbolicSet sspost(abstraction_d1->getStateSpacePost());
            
            scots::FixedPoint fp(abstraction_d1);
            int size = ranking_function.size();

            //Cut outgoing transitions of already computed resiliences to make them unreachable in the parity game, due to finite traces.
            abstraction_d1->setTransitionRelation(abstraction_d1->getTransitionRelation().getSymbolicSet() & (!ranking_function[size-1]->getSymbolicSet()));
            scots::FixedPoint fp_c(abstraction_d1);
            BDD W0_ = fp_c.parity((*c0).getSymbolicSet(), (*c1).getSymbolicSet(), (*c2).getSymbolicSet(), (*c3).getSymbolicSet());
            BDD W0 = W0_.ExistAbstract(is.getCube());
            scots::SymbolicSet W1(ss);
            W1.setSymbolicSet((!W0) & abstraction_d1->getStateSpace().getSymbolicSet());
            W1.writeToFile("W1_risk_update.bdd"); //Gives 0. Just see what happens when we complement //! gives {4,5,2,3} which is perfect. Now just intersect with rf[1] viz{5} to get resil OR JUST UNION IT (PROVE THIS!!)!

            BDD RF = ranking_function[size-1]->getSymbolicSet() | W1.getSymbolicSet();
            ranking_function[size-1] = new scots::SymbolicSet(ss); 
            ranking_function[size-1]->setSymbolicSet(RF);

            //Update resilient controller: Previous controller W0:Strategy (from last risk update stored in controller_intermediate) but for W1 states from current risk update stored in ranking_function[size-1]
            controller_resilient->setSymbolicSet(controller_resilient->getSymbolicSet() | (controller_intermediate->getSymbolicSet() &  ranking_function[size-1]->getSymbolicSet()));
            
            //Backup current controller (W0_) for future risk updates
            controller_intermediate->setSymbolicSet((W0_) & abstraction_d1->getStateSpace().getSymbolicSet());
            controller_intermediate->writeToFile("controller_intermediate.bdd");

            //Conclusion: Game cutoff is not needed for computing correct controller. It is just an optimisation step. Hence, proof should go through with just strategy pruning and disturbance update over entire graph! :) 
            //In fact, just pruning strategy off abstraction_d1 (normal transition's) is enough (necessary and sufficient)
            }

            void printResiliences()
            {
                int size = ranking_function.size();

                controller_resilient->writeToFile("controller_resilient.bdd");
                std::cout << "Highest resilience is:" << size-1 <<endl;

                for(int i=0; i<size; i++)
                {
                    std::string res("r");
                    res.append(std::to_string(i));
                    res.append(".bdd");
                    
                    int n = res.length();   
                    // declaring character array 
                    char char_array[n + 1];            
                    // copying the contents of the 
                    // string to char array 
                    strcpy(char_array, res.c_str()); 

                    ranking_function[i]->writeToFile(char_array);
                }         
            }
    };
}