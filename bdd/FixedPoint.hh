/*
 * FixedPoint.hh
 *
 *  created on: 09.10.2015
 *      author: rungger
 */

#ifndef FIXEDPOINT_HH_
#define FIXEDPOINT_HH_

#include <iostream>
#include <stdexcept>

#include "cuddObj.hh"
#include "SymbolicModel.hh"


namespace scots {
/*
 * class: FixedPoint
 *
 * 
 * it provides  some minimal and maximal fixed point computations to 
 * synthesize controllers for simple invarinace and reachability 
 * spezifications
 *
 */
class FixedPoint {
protected:
  /* var: ddmgr_ */
  Cudd *ddmgr_;
  /* var: symbolicModel_ 
   * stores the transition relation */
  SymbolicModel *symbolicModel_;
  /* var: permute 
   * stores the permutation array used to swap pre with post variables */
  int* permute_;

  /* helper BDDs */
  /* transition relation */
  BDD R_;  
  /* transition relation with cubePost_ abstracted */
  BDD RR_;  

   /* cubes with input and post variables; used in the existential abstraction  */
  BDD cubePost_;
  BDD cubeInput_;
  // friend class Resilience;
public:

  /* function: FixedPoint 
   *
   * initialize the FixedPoint object with a <SymbolicModel> containing the
   * transition relation
   */
  FixedPoint(SymbolicModel *symbolicModel) {
     symbolicModel_=symbolicModel;
    ddmgr_=symbolicModel_->ddmgr_;
     /* the permutation array */
    size_t n=ddmgr_->ReadSize();
    permute_ = new int[n];
    for(size_t i=0; i<n; i++)
      permute_[i]=i;
    for(size_t i=0; i<symbolicModel_->nssVars_; i++)
      permute_[symbolicModel_->preVars_[i]]=symbolicModel_->postVars_[i];
    /* create a cube with the input Vars */
    BDD* vars = new BDD[symbolicModel_->nisVars_];
    for (size_t i=0; i<symbolicModel_->nisVars_; i++)
      vars[i]=ddmgr_->bddVar(symbolicModel_->inpVars_[i]);
    cubeInput_ = ddmgr_->bddComputeCube(vars,NULL,symbolicModel_->nisVars_);
    delete[] vars;
    /* create a cube with the post Vars */
    vars = new BDD[symbolicModel_->nssVars_];
    for (size_t i=0; i<symbolicModel_->nssVars_; i++)
      vars[i]=ddmgr_->bddVar(symbolicModel_->postVars_[i]);   
    cubePost_ = ddmgr_->bddComputeCube(vars,NULL,symbolicModel_->nssVars_);
    delete[] vars;

    /* copy the transition relation */
    R_=symbolicModel_->transitionRelation_;
    RR_=R_.ExistAbstract(cubePost_);
  }
  ~FixedPoint() {
    delete[] permute_;
  }

  /* function: pre 
   *
   * computes the enforcable predecessor 
   *  
   * pre(Z) = { (x,u) | exists x': (x,u,x') in transitionRelation 
   *                    and (x,u,x') in transitionRelation  => x' in Z } 
   *
   */
  BDD pre(BDD Z)  {
    /* project onto state alphabet */
    Z=Z.ExistAbstract(cubePost_*cubeInput_);
    
    /* swap variables */
    Z=Z.Permute(permute_);
    /* find the (state, inputs) pairs with a post outside the safe set */

    BDD nZ = !Z;
    BDD F = R_.AndAbstract(nZ,cubePost_); 
    /* the remaining (state, input) pairs make up the pre */
    BDD nF = !F;
    BDD preZ= RR_.AndAbstract(nF,cubePost_);

    return preZ;
  }

  /* function: uPre 
   *
   * computes the cooperative predecessor 
   *  
   * uPre(Z) = { (x,u) | exists x': ((x,u,x') in transitionRelation 
   *                                  and x' in Z) } 
   *
   */
  /*
  Author: Stanly Samuel, 2020, RESCOT.
  */
    BDD uPre(BDD Z) {
      /* project onto state alphabe */
      Z=Z.ExistAbstract(cubePost_*cubeInput_);
      /* swap variables */
      Z = Z.Permute(permute_);
      /* find the (state,input) pairs with a post inside Z */
      BDD uPreZ = R_.AndAbstract(Z, cubePost_);
      return uPreZ;
    }

  /* function: subGraph 
   *
   *  Removes the set Z from transition R_
   *  Removes the outgoing and incoming transitions to Z in R_
   */
  /*
  Author: Stanly Samuel, 2020, RESCOT.
  */
    BDD subGraph(BDD Z)
    {
      //Remove outgoing transitions from states in Z.
      //This suffices because they will be dead states anyway and will play no part in the solution of any game as they result in a finite trace.
      Z=Z.ExistAbstract(cubePost_*cubeInput_);
      R_ = R_ * (!Z);
      /* swap variables */
      Z=Z.Permute(permute_);
      //Remove incoming transitions. Removes redundant transitions in the BDD.
      R_ = R_ & (!Z);
      return R_;                   
    }

  /* function: safe 
   *  
   * computation of the maximal fixed point mu Z.pre(Z) & S
   *
   */
  BDD safe(BDD S, int verbose=0)  {
    /* check if safe is a subset of the state space */
    std::vector<unsigned int> sup = S.SupportIndices();
    for(size_t i=0; i<sup.size();i++) {
      int marker=0;
      for(size_t j=0; j<symbolicModel_->nssVars_; j++) {
        if (sup[i]==symbolicModel_->preVars_[j])
          marker=1;
      }
      if(!marker) {
          std::ostringstream os;
          os << "Error: safe: the inital set depends on variables  outside of the state space.";
          throw std::invalid_argument(os.str().c_str());
      }
    }
    if(verbose) 
      std::cout << "Iterations: ";

    BDD Z = ddmgr_->bddZero();
    BDD ZZ = ddmgr_->bddOne();
    /* as long as not converged */
    size_t i;
    for(i=1; ZZ != Z; i++ ) {
      Z=ZZ;
      ZZ=FixedPoint::pre(Z) & S;

      /* print progress */
      if(verbose) {
        std::cout << ".";
        std::flush(std::cout);
        if(!(i%80))
          std::cout << std::endl;
      }
    }
    if(verbose) 
      std::cout << " number: " << i << std::endl;
    return Z;
  }  
  
  /* function: reach 
   *  
   * computation of the minimal fixed point mu Z.pre(Z) | T
   *
   */
  BDD reach(const BDD &T, int verbose=0)  {
    /* check if target is a subset of the state space */
    std::vector<unsigned int> sup = T.SupportIndices();
    for(size_t i=0; i<sup.size();i++) {
      int marker=0;
      for(size_t j=0; j<symbolicModel_->nssVars_; j++) {
        if (sup[i]==symbolicModel_->preVars_[j])
          marker=1;
      }
      if(!marker) {
        std::ostringstream os;
        os << "Error: reach: the target set depends on variables outside of the state space.";
        throw std::invalid_argument(os.str().c_str());
      }
    }
    if(verbose) 
      std::cout << "Iterations: ";

    BDD Z = ddmgr_->bddOne();
    BDD ZZ = ddmgr_->bddZero();
    /* the controller */
    BDD C = ddmgr_->bddZero();
    /* as long as not converged */
    size_t i;
    for(i=1; ZZ != Z; i++ ) {
      Z=ZZ;
      ZZ=FixedPoint::pre(Z) | T;
      /* new (state/input) pairs */
      BDD N = ZZ & (!(C.ExistAbstract(cubeInput_)));
      /* add new (state/input) pairs to the controller */
      C=C | N;
      /* print progress */
      if(verbose) {
        std::cout << ".";
        std::flush(std::cout);
        if(!(i%80))
          std::cout << std::endl;
      }
    }
    if(verbose) 
      std::cout << " number: " << i << std::endl;
    return C;
  }

  /* function: reachAvoid 
   *  
   * controller synthesis to enforce reach avoid specification
   *
   * computation of the minimal fixed point mu Z.(pre(Z) | T) & !A
   *
   */
  BDD reachAvoid(const BDD &T, const BDD &A, int verbose=0)  {
    /* check if target is a subset of the state space */
    std::vector<unsigned int> sup = T.SupportIndices();
    for(size_t i=0; i<sup.size();i++) {
      int marker=0;
      for(size_t j=0; j<symbolicModel_->nssVars_; j++) {
        if (sup[i]==symbolicModel_->preVars_[j])
          marker=1;
      }
      if(!marker) {
          std::ostringstream os;
          os << "Error: reachAvoid: the inital set depends on variables  outside of the state space.";
          throw std::invalid_argument(os.str().c_str());
      }
    }
    if(verbose) 
      std::cout << "Iterations: ";

    BDD RR=RR_;
    /* remove avoid (state/input) pairs from transition relation */
    RR_= RR & (!A);
    BDD TT= T & (!A);

    BDD Z = ddmgr_->bddOne();
    BDD ZZ = ddmgr_->bddZero();
    /* the controller */
    BDD C = ddmgr_->bddZero();
    /* as long as not converged */
    size_t i;
    for(i=1; ZZ != Z; i++ ) {
      Z=ZZ;
      ZZ=FixedPoint::pre(Z) | TT;
      /* new (state/input) pairs */
      BDD N = ZZ & (!(C.ExistAbstract(cubeInput_)));
      /* add new (state/input) pairs to the controller */
      C=C | N;
      /* print progress */
      if(verbose) {
        std::cout << ".";
        std::flush(std::cout);
        if(!(i%80))
          std::cout << std::endl;
      }
    }
    if(verbose) 
      std::cout << " number: " << i << std::endl;
    /* restor transition relation */
    RR_=RR;
    return C;
  }

  /* function: 4-color parity
   *  
   * controller synthesis to enforce parity specification
   *
   * computation of mu.W nu.X mu.Y nu.Z (pre(W) & C3)| (pre(X) & C2) | (pre(Y) & C1)| (pre(Z) & C0)
   *
   */
  /*
  Author: Stanly Samuel, 2020, RESCOT.
  */

  BDD parity(const BDD &C0, const BDD &C1, const BDD &C2, const BDD &C3)  {

    //Outermost C represents the final controller but X, inner C and Z are also respective controllers of safety, reachability and safety games respectively which are passed to the outer loops at every stage.
    BDD W = ddmgr_->bddOne(); //C3
    BDD WW = ddmgr_->bddZero();
    
    BDD C = ddmgr_->bddZero();
    for(size_t l=1; WW!=W; l++) 
    {
      W=WW;
      BDD X = ddmgr_->bddZero(); //C2
      BDD XX = ddmgr_->bddOne();
      /* as long as not converged */    
      size_t i;
      for(i=1; XX != X; i++ )  
      {
        X=XX;
        BDD Y = ddmgr_->bddOne();//C1
        BDD YY = ddmgr_->bddZero();
        
        BDD C = ddmgr_->bddZero();  //Create a fresh controller for C1 computation and return its results to the outer loop
        for(size_t j=1; YY != Y; j++ )
          {
            Y=YY;
            BDD Z = ddmgr_->bddZero(); //C0
            BDD ZZ = ddmgr_->bddOne();
            /* as long as not converged */    
            for(size_t k=1; ZZ != Z; k++ ) {  
              Z=ZZ;
              ZZ= (FixedPoint::pre(W) & C3) | (FixedPoint::pre(X) & C2) | (FixedPoint::pre(Y) & C1 )| (FixedPoint::pre(Z) & C0);
              std::cout << "0."; 
            } //C0 END: return Z
            YY = Z;
            BDD N = YY & (!(C.ExistAbstract(cubeInput_)));
            /* add new (state/input) pairs to the controller */
            C=C | N;
            std::cout << "1.";
          }//C1 END: return (inner) C
          XX = C;
          std::cout << "2.";
      } //C2 END: return X
      WW=X;
      //WW = XX == YY == ZZ == W == X == Y == Z at this point (EACH OF THEM ARE CONTROLLERS!)
      BDD N = WW & (!(C.ExistAbstract(cubeInput_)));
      /* add new (state/input) pairs to the controller */
      C=C | N;
      std::cout << "3.";
    } //C3 END: return C
    return C;
  }

}; /* close class def */
} /* close namespace */

#endif /* FIXEDPOINT_HH_ */
