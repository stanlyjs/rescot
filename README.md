# Welcome to RESCOT v1.0 (Portmanteau for Resilient SCOTS)

**SCOTS**, by Matthias Rungger et al., is a C++ tool (with a small Matlab interface) to synthesize controllers for
possibly perturbed nonlinear control systems with respect to safety and reachability specifications.
SCOTS can be downloaded at https://www.hcs.ei.tum.de/en/software/scots/.

**RESCOT** builds upon SCOTS by adding the notion of resilience as defined in the paper [Synthesizing Optimally Resilient Controllers](https://www.react.uni-saarland.de/publications/robustparity.pdf) by Neider et. al.

**RESCOT** is based on the paper [Resilient Abstraction-Based Controller Design](https://people.mpi-sws.org/~kmallik/uploads/ResilientABCD2020.pdf) by Samuel et. al, currently under submission for CDC 2020.

**To replicate the results in the HSCC 2020 poster and the CDC 2020 submitted paper**, please install the tool using the instructions given below and then follow the instructions in ./rescot_examples/CDC2020/README.

If you face any issues, please contact Stanly Samuel (stanly@iisc.ac.in).

### Requirements

- A C++ development environment where you can compile C++ source code.

- The CUDD library by Fabio Somenzi, which can be downloaded at
    http://vlsi.colorado.edu/~fabio/. 
    SCOTS uses the dddmp and C++ wrapper of the CUDD library.

    In the example computations we used cudd-3.0.0 which we configured with 

    `$./configure --enable-shared --enable-obj --enable-dddmp --prefix=/opt/local/`
    and
    `$autoreconf`

- RESCOT is a header only library. You need only add the SCOTS source
  directory to the include directory in the compiler command. 

    Further details are found in the readme files in example directories and in the manual.

### Directory structure

- ./bdd/
    Contains the source C++ source code for the SCOTS, RESCOT classes 
    which use Binary Decision Diagrams as the underlying data structure
  
- ./scots_examples/
    Some C++/Matlab programs demonstrating the usage of basic SCOTS
  
- ./rescot_examples/
    Examples demonstrating the usage and performance of RESCOT

- ./rescot_examples/CDC2020/
    Examples in CDC 2020 submitted paper, for replication
  
- ./mfiles
    Contains an mfile as a wrapper to the mex-file functions
  
- ./mfiles/mexfiles/
    mex-file to read the C++ output from file 