REPLICATING RESULTS FOR CDC 2020

You may run the unicycle_with_resilience.cc file in every folder again as per directions in SCOTS manual to get the .bdd files and resilience values as output.
You can directly run the simulation (unicycle_with_resilience.m) in MATLAB assuming cudd is installed and the mexfiles are generated as per the directions in SCOTS manual.

----------------------------------------------------------------------

Before compiling and executing unicycle_with_resilience.cc, you can change the higher disturbance spikes values to the ones given in the paper (E.x. 0.5 and 2 for Fig1 mid and right
 ) to get the results in the paper.
While simulating with unicycle_with_resilience.m, please update the resilience value that you get in the previous step, in line 40 'max_resilience = _'. Also, use the same disturbance values that you used in unicycle_with_resilience.cc to unicycle_with_resilience.m's lines 161 and 162.
----------------------------------------------------------------
For example, in unicycle_with_resilience.cc, if you used:
    #define lower_disturbance 0.05
    #define upper_disturbance 1
    and got the finite resilience value as 7
then in unicycle_with_resilience.m, use:
    wlb=[0.05;0.05]; % upper bound on the normal disturbance
    wub=[1;1]; % upper bound on disturbance spikes 
    and
    max_resilience = 7;
