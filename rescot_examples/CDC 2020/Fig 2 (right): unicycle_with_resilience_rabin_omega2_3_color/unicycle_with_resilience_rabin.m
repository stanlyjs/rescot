%
% sample.m
%
% created on: 13.9.2019
%     author: Stanly
%
% see readme file for more information on the sample example
%
% you need to run ./sample binary first 
%
% so that the files: 
%                    sample_ss.bdd 
%                    sample_obst.bdd
%                    sample_target.bdd
%                    sample_controller.bdd 
% are created
%

ss=SymbolicSet('ss.bdd'); 
is=SymbolicSet('is.bdd');

% abstraction_d1=SymbolicSet('abstraction_d1.bdd');
% abstraction_d2=SymbolicSet('abstraction_d2.bdd');
% abstraction_d=SymbolicSet('abstraction_d.bdd');

c0=SymbolicSet('c0.bdd');
c1=SymbolicSet('c1.bdd');
c2=SymbolicSet('c2.bdd');

% controller=SymbolicSet('controller_intermediate.bdd');
% % adversary_controller=SymbolicSet('adversary_controller.bdd');
W0=SymbolicSet('W0.bdd');
W1=SymbolicSet('W1.bdd');

% controller_rigged=SymbolicSet('controller_rigged.bdd');
omega1ResStates=SymbolicSet('omega1ResStates.bdd');
omegaResStates=SymbolicSet('omegaResStates.bdd');

max_resilience = 7;

% create a default color map ranging from red to light pink
length = max_resilience+1;
red = [1, 0, 0];
pink = [255, 192, 203]/255;
colors_p = [linspace(red(1),pink(1),length)', linspace(red(2),pink(2),length)', linspace(red(3),pink(3),length)'];


%% plot the unicycle domain
% colors
colors=get(groot,'DefaultAxesColorOrder');

% load the symbolic set containig the abstract state space
set=SymbolicSet('ss.bdd','projection',[1 2]);
plotCells(set,'facecolor','none','edgec',[0.8 0.8 0.8],'linew',.1)
hold on

% load the symbolic set containig c0
set=SymbolicSet('c0.bdd','projection',[1 2]);
plotCells(set,'facecolor',colors(4,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)

% load the symbolic set containig c1
set=SymbolicSet('c1.bdd','projection',[0 1]);
% plotCells(set,'facecolor',colors(3,:)*0.5+0.5,'edgec',colors(3,:),'linew',.1)

% load the symbolic set containig c2
set=SymbolicSet('c2.bdd','projection',[0 1]);
plotCells(set,'facecolor',colors(4,:)*0.5+0.5,'edgec',colors(4,:),'linew',.1)

%load the symbolic set containig r0
set=SymbolicSet('W1.bdd','projection',[1 2]);
 %plotCells(set,'facecolor',colors(1,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)

% for i = 1:max_resilience
%     str= ['r' num2str(i) '.bdd'];
%         % load the symbolic set containig ri
%     set=SymbolicSet(str,'projection',[1 2]);
%     plotCells(set,'facecolor',colors_p(i,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)
% end

% load the symbolic set containig w+1 states
 set=SymbolicSet('omega1ResStates.bdd','projection',[1 2]);
  plotCells(set,'facecolor',colors(5,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)

box on
axis([0.5 7.5 0.5 10.5])

%% target set
L=[2 0 0; 0 1 0; 0 0 .1];
c=[5.1; 2.3; 0];

% initial state
x0=[1.9 5.5 pi/2];

controller=SymbolicSet('controller_resilient.bdd');


i=0;

y=x0;
while(i<50)

%     if(omega1ResStates.isElement(y(end,:)))
%       count_success = count_success+1;
%       break;
%     end
%     if(obst.isElement(y(end,:)))
%       count_crash = count_crash+1;
%       break;
%     end
    
     u=controller.getInputs(y(end,:));
%       if(rand<0.5)
%     u=controller.getInputs(y(end,:));
%   else
%     u=abstraction_d.getInputs(y(end,:));
%     u(:,3:5) = []; %The value of columns (6:8) is specific to this example
%end
    % y on input u gives non det x values. 
    [t x]=ode45(@unicycle_ode,[0 .3], y(end,:),[],u(1,:),d);
    % Pick last x in the list and choose that as the next state and append to the list
    y=[y; x(end,:)];
  
    % Plot black trajectory with green marker
    %Black path
    plot(y(:,1),y(:,2),'k.-')
    %Green marker
    h = plot(y(end,1),y(end,2),'.','color',colors(5,:),'markersize',20)
    pause(0.1);
	delete(h);
i = i+1
end

% initial state
x0=[6 2 pi/2];

controller=SymbolicSet('controller_resilient.bdd');


i=0;

y=x0;
while(i<50)

%     if(omega1ResStates.isElement(y(end,:)))
%       count_success = count_success+1;
%       break;
%     end
%     if(obst.isElement(y(end,:)))
%       count_crash = count_crash+1;
%       break;
%     end
    
     u=controller.getInputs(y(end,:));
%       if(rand<0.5)
%     u=controller.getInputs(y(end,:));
%   else
%     u=abstraction_d.getInputs(y(end,:));
%     u(:,3:5) = []; %The value of columns (6:8) is specific to this example
%end
    % y on input u gives non det x values. 
    [t x]=ode45(@unicycle_ode,[0 .3], y(end,:),[],u(1,:),d);
    % Pick last x in the list and choose that as the next state and append to the list
    y=[y; x(end,:)];
  
    % Plot black trajectory with green marker
    %Black path
    plot(y(:,1),y(:,2),'k.-')
    %Green marker
    h = plot(y(end,1),y(end,2),'.','color',colors(5,:),'markersize',20)
    pause(0.1);
	delete(h);
i=i+1
end

function dxdt = unicycle_ode(t,x,u,d)

dxdt = zeros(3,1);

wlb=[0;0]; % upper bound on the normal disturbance
wub=[1;1]; % upper bound on disturbance spikes

% with 70% probability, w is within the normal range (i.e. within [-wlb wlb]), and with 30% probability, w is in the spike (i.e. either within [-wub -wlb] or within [wlb wub]
 r1=rand;

if (r1<=0.7)
w(1)=2*wlb(1)*rand - wlb(1);
w(2)=2*wlb(2)*rand - wlb(2);
else
w(1)=(wub(1)-wlb(1))*rand + wlb(1);
r2 = rand;
if (r2<0.5)
w(1)=-w(1);
end
w(2)=(wub(2)-wlb(2))*rand + wlb(2);
r2 = rand;
if (r2<0.5)
w(2)=-w(2);
end
end           

r=rand;
if(r<1)
     dxdt(1)=u(1)*cos(x(3))+w(1);
     dxdt(2)=u(1)*sin(x(3))+w(2);
     dxdt(3)=u(2);
else
     dxdt(1)=u(1)*cos(x(3));
     dxdt(2)=u(1)*sin(x(3));
     dxdt(3)=u(2);
end
end


