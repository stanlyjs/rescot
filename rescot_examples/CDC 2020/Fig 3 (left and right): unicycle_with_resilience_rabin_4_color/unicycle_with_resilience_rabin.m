%
% sample.m
%
% created on: 13.9.2019
%     author: Stanly
%
% see readme file for more information on the sample example
%
% you need to run ./sample binary first 
%
% so that the files: 
%                    sample_ss.bdd 
%                    sample_obst.bdd
%                    sample_target.bdd
%                    sample_controller.bdd 
% are created
%

ss=SymbolicSet('ss.bdd'); 
obst=SymbolicSet('obst.bdd');
is=SymbolicSet('is.bdd');
% abstraction_d1=SymbolicSet('abstraction_d1.bdd');
% abstraction_d2=SymbolicSet('abstraction_d2.bdd');
% abstraction_d=SymbolicSet('abstraction_d.bdd');

c0=SymbolicSet('c0.bdd');
c1=SymbolicSet('c1.bdd');
c2=SymbolicSet('c2.bdd');
c3=SymbolicSet('c3.bdd');

% controller=SymbolicSet('controller_intermediate.bdd');
% % adversary_controller=SymbolicSet('adversary_controller.bdd');
% W0=SymbolicSet('W0.bdd');
% W1=SymbolicSet('W1.bdd');

% r0=SymbolicSet('r0.bdd');
% r1=SymbolicSet('r1.bdd');
% r2=SymbolicSet('r2.bdd');

% r0_u=SymbolicSet('r0_u.bdd');
% r1_u=SymbolicSet('r1_u.bdd');
% r2_u=SymbolicSet('r2_u.bdd');

% controller_rigged=SymbolicSet('controller_rigged.bdd');
 omega1ResStates=SymbolicSet('omega1ResStates.bdd');
 omegaResStates=SymbolicSet('omegaResStates.bdd');

max_resilience = 3;

% create a default color map ranging from red to light pink
length = max_resilience+1;
red = [1, 0, 0];
pink = [255, 192, 203]/255;
colors_p = [linspace(red(1),pink(1),length)', linspace(red(2),pink(2),length)', linspace(red(3),pink(3),length)'];


%% plot the unicycle domain
% colors
colors=get(groot,'DefaultAxesColorOrder');

% load the symbolic set containig the abstract state space
set=SymbolicSet('ss.bdd','projection',[1 2]);
plotCells(set,'facecolor','none','edgec',[0.8 0.8 0.8],'linew',.1)
hold on


 

% load the symbolic set containig c1
% set=SymbolicSet('c1.bdd','projection',[0 1]);
% plotCells(set,'facecolor',colors(3,:)*0.5+0.5,'edgec',colors(3,:),'linew',.1)

%load the symbolic set containig c3
% set=SymbolicSet('c3.bdd','projection',[0 1]);
% plotCells(set,'facecolor',colors(4,:)*0.5+0.5,'edgec',colors(4,:),'linew',.1)

 % load the symbolic set containig r15
% set=SymbolicSet('r15.bdd','projection',[1 2]);
% plotCells(set,'facecolor',colors(1,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)

for i = 1:max_resilience
    str= ['r' num2str(i) '.bdd'];
        % load the symbolic set containig ri
    set=SymbolicSet(str,'projection',[1 2]);
    plotCells(set,'facecolor',colors_p(i,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)
end
% load the symbolic set containig c2
 set=SymbolicSet('c2.bdd','projection',[1 2]);
 plotCells(set,'facecolor',colors(4,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)

% load the symbolic set containig c0
 set=SymbolicSet('c0.bdd','projection',[1 2]);
 plotCells(set,'facecolor',colors(4,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)
 
% load the symbolic set containig omegaRes1States
 set=SymbolicSet('omega1ResStates.bdd','projection',[1 2]);
 plotCells(set,'facecolor',colors(4,:)*0.5+0.5,'edgec',colors(2,:),'linew',.1)

% load the symbolic set containig obstacles
set=SymbolicSet('obst.bdd','projection',[1 2]);
plotCells(set,'facecolor',colors(1,:)*0.5+0.5,'edgec',colors(1,:),'linew',.1)

 v=[3   0  ;3.2  0   ; 3     2    ; 3.2 2   ];
 patch('vertices',v,'faces',[1 2 4 3],'facec',colors(1,:),'edgec',colors(1,:));
 v=[3   3  ;3.2  3   ; 3   6    ; 3.2 6   ];
 patch('vertices',v,'faces',[1 2 4 3],'facec',colors(1,:),'edgec',colors(1,:));

 v=[7   4  ;7.2  4   ; 7     6    ; 7.2 6   ];
 patch('vertices',v,'faces',[1 2 4 3],'facec',colors(1,:),'edgec',colors(1,:));
 v=[7   7  ;7.2  7   ; 7   10    ; 7.2 10   ];
 patch('vertices',v,'faces',[1 2 4 3],'facec',colors(1,:),'edgec',colors(1,:));

 v=[8.3   1.9  ;9.1  1.9   ; 8.3   4.1    ; 9.1 4.1   ];
 patch('vertices',v,'faces',[1 2 4 3],'facec',colors(1,:),'edgec',colors(1,:));
 
box on
axis([-.5 10.5 -.5 10.5])

%% target set
L=[2 0 0; 0 1 0; 0 0 .1];
c=[9.1; 8.7; 0];

% initial state
x0=[7.3 3.1 pi/2];

controller=SymbolicSet('controller_resilient.bdd');
y=x0;
v=[];

while(1)

  %Specific to reachability. Just checks if reached target. May have to
  %change for others.Cos it depends on L.
%   if ( (y(end,:)-c')*L'*L*(y(end,:)'-c)<=1 )
%     break;
%   end
%     if(omega1ResStates.isElement(y(end,:)))
%       break;
%     end
    if(obst.isElement(y(end,:)))
      break;
    end
    

%   if(rand<0.5)
    u=controller.getInputs(y(end,:));
%   else
%     u=abstraction_d.getInputs(y(end,:));
%     u(:,3:5) = []; %The value of columns (6:8) is specific to this example
%   end
  v=[v; u(1,:)];
  [t x]=ode45(@unicycle_ode,[0 .3], y(end,:),[],u(1,:));

  y=[y; x(end,:)];
  
    % Plot black trajectory with green marker
    %Black path
    plot(y(:,1),y(:,2),'k.-')
    %Green marker
    h = plot(y(end,1),y(end,2),'.','color',colors(5,:),'markersize',20)
    pause(0.1);
    delete(h);
end



% function dxdt = unicycle_ode(t,x,u)

%   dxdt = zeros(3,1);

%   dxdt(1)=u(1)*cos(x(3));
%   dxdt(2)=u(1)*sin(x(3));
%   dxdt(3)=u(2);


% end

function dxdt = unicycle_ode(t,x,u)

  dxdt = zeros(3,1);
  
  wlb=[0;0]; % upper bound on the normal disturbance
  wub=[2;2]; % upper bound on disturbance spikes
  
  % with 70% probability, w is within the normal range (i.e. within [-wlb wlb]), and with 30% probability, w is in the spike (i.e. either within [-wub -wlb] or within [wlb wub]
  r1=rand;
  
  if (r1<=0.1)
  w(1)=2*wlb(1)*rand - wlb(1);
  w(2)=2*wlb(2)*rand - wlb(2);
  else
  w(1)=(wub(1)-wlb(1))*rand + wlb(1);
  r2 = rand;
  if (r2<0.5)
  w(1)=-w(1);
  end
  w(2)=(wub(2)-wlb(2))*rand + wlb(2);
  r2 = rand;
  if (r2<0.5)
  w(2)=-w(2);
  end
  end           
  
%   r=rand;
%   if(r<0.2)
       dxdt(1)=u(1)*cos(x(3))+w(1);
       dxdt(2)=u(1)*sin(x(3))+w(2);
       dxdt(3)=u(2);
%   else
%        dxdt(1)=u(1)*cos(x(3));
%        dxdt(2)=u(1)*sin(x(3));
%        dxdt(3)=u(2);
%   end
  end


