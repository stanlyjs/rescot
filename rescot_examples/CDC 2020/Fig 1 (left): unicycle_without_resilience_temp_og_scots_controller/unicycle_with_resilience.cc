/*
 * unicycle_with_resilience_og_temp.cc
 */

#include <array>
#include <iostream>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
#include "SymbolicModelGrowthBound.hh"
#include "Resilience.hh"

#include "TicToc.hh"
#include "FixedPoint.hh"
#include "RungeKutta4.hh"

/* state space dim */
#define sDIM 3
#define iDIM 2

typedef std::array<double,3> state_type;
typedef std::array<double,2> input_type;

/*--------------------------------------------------------------------------------------------------*/

/* State space eta */
#define etaX0 0.2
#define etaX1 0.2
#define etaX2 0.1

/* Input space eta */
#define etaU0 0.3
#define etaU1 0.2

/* sampling time */
const double tau = 0.3;
/* number of intermediate steps in the ode solver */
const int nint=5;
OdeSolver ode_solver(sDIM,nint,tau);

/*--------------------------------------------------------------------------------------------------*/

/* we integrate the unicycle ode by 0.3 sec (the result is stored in x)  */
auto  unicycle_post = [](state_type &x, input_type &u) -> void {

  /* the ode describing the unicycle */
  auto rhs =[](state_type& xx,  const state_type &x, input_type &u) -> void {
      xx[0] = u[0]*std::cos(x[2]);
      xx[1] = u[0]*std::sin(x[2]);
      xx[2] = u[1];
  };
  ode_solver(rhs,x,u);
};

/* computation of the growth bound (the result is stored in r)  */
auto radius_post = [](state_type &r, input_type &u) -> void {
    r[0] = r[0]+r[2]*std::abs(u[0])*0.3;
    r[1] = r[1]+r[2]*std::abs(u[0])*0.3;
};

int main() {
  /* to measure time */
  TicToc tt;
  /* there is one unique manager to organize the bdd variables */
  Cudd mgr;

  /****************************************************************************/
  /* construct SymbolicSet for the state space */
  /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /*state space */
    /* lower bounds of the hyper rectangle */
    double lbX[sDIM]={1,1,-M_PI-0.4};
    /* upper bounds of the hyper rectangle */
    double ubX[sDIM]={7,10,M_PI+0.4};
    /* grid node distance diameter */
    double etaX[sDIM]={etaX0, etaX1, etaX2};

    scots::SymbolicSet ss(mgr,sDIM,lbX,ubX,etaX);
    
    /* add the grid points to the SymbolicSet ss */
    ss.addGridPoints();
    ss.writeToFile("ss.bdd");

    //Create a separate set for polytopes over the same state space.
    scots::SymbolicSet ss_obst(ss);
    double Ho[4*sDIM]={-1, 0, 0,
                1, 0,0,
                0,-1,0,
                0, 1,0};
    /* remove outer approximation of P={ x | H x<= h1 } form state space */
    double h1[4] = {-3,3.2,-1, 2};
    ss_obst.addPolytope(4,Ho,h1, scots::OUTER);
    // ss.remPolytope(4,Ho,h1, scots::OUTER);

    /* remove outer approximation of P={ x | H x<= h1 } form state space */
    double h2[4] = {-3,3.2,-3, 6};
    ss_obst.addPolytope(4,Ho,h2, scots::OUTER);
    // ss.remPolytope(4,Ho,h2, scots::OUTER);

    scots::SymbolicSet ss_obst_border(ss);
    /* remove outer approximation of P={ x | H x<= h1 } form state space */
    double h3[4] = {-1.3,6.7,-1.3, 9.7};
    ss_obst_border.addPolytope(4,Ho,h3, scots::OUTER);
    
    ss_obst_border.setSymbolicSet(ss.getSymbolicSet() & !ss_obst_border.getSymbolicSet());

    ss_obst.setSymbolicSet(ss_obst.getSymbolicSet() | ss_obst_border.getSymbolicSet());
  
    //We don't remove the polytopes but assign them color one.
    ss_obst.writeToFile("obst.bdd");
    // ss.complement();
    // ss.writeToFile("obst.bdd");
    // ss.complement();
    std::cout << "Unfiorm grid details:" << std::endl;
    ss.printInfo(1);

  /****************************************************************************/
  /* construct SymbolicSet for the input space */
  /****************************************************************************/
    /* lower bounds of the hyper rectangle */
    double lbU[sDIM]={-1,-1.5};  
    // double lbU[sDIM]={-0.5,-1};  
    /* upper bounds of the hyper rectangle */
    double ubU[sDIM]={1,1.5}; 
    /* grid node distance diameter */
    double etaU[sDIM]={etaU0, etaU1};   
      
    scots::SymbolicSet is(mgr,iDIM,lbU,ubU,etaU);
    is.addGridPoints();
    std::cout << std::endl << "Input space details:" << std::endl;
    is.printInfo(1);
    is.writeToFile("is.bdd");


  /****************************************************************************/
  /* C0 */
  /****************************************************************************/
  /* first make a copy of the state space so that we obtain the grid
   * information in the new symbolic set */

  scots::SymbolicSet c0 = ss;
  /* define the target set as a symbolic set */
  double Ha[9]={ 2, 0, 0,
                0, 1, 0,
                0, 0, .1};
  /* compute inner approximation of P={ x | H x<= h1 }  */
  double c[3] = {5.1,2.3,0};
  c0.addEllipsoid(Ha,c, scots::INNER);
  c0.writeToFile("c0.bdd");

 /****************************************************************************/
  /* C1 */
  /****************************************************************************/
  scots::SymbolicSet c1 = ss;
  c1.setSymbolicSet(ss.getSymbolicSet() & !(c0.getSymbolicSet()));
  c1.writeToFile("c1.bdd"); 

  /****************************************************************************/
  /* C2 */
  /****************************************************************************/
  /* Remaining color C2 */

  scots::SymbolicSet c2 = ss;
  c2.setSymbolicSet(mgr.bddZero()); 
  c2.writeToFile("c2.bdd"); 

  /****************************************************************************/
  /* C3 */
  /****************************************************************************/
  /* Remaining color C3 */

  scots::SymbolicSet c3 = ss;
  c3.setSymbolicSet(mgr.bddZero()); 
  c3.writeToFile("c3.bdd"); 

  /****************************************************************************/
  /* setup class for symbolic model computation */
  /****************************************************************************/
  /* first create SymbolicSet of post variables 
   * by copying the SymbolicSet of the state space and assigning new BDD IDs */
  scots::SymbolicSet sspost(ss,1);

  //States where while computing abstraction, you want to add self loops
  scots::SymbolicSet self_loop(ss_obst);
  self_loop.setSymbolicSet(ss_obst.getSymbolicSet() | c0.getSymbolicSet());


 /* setup class for symbolic model computation for lower value of disturbances */

  /* instantiate the SymbolicModel */
  scots::SymbolicModelGrowthBound<state_type,input_type> abstraction_d1(&ss, &is, &sspost);

  /* compute the transition relation */
  tt.tic();
  abstraction_d1.computeTransitionRelation(unicycle_post, radius_post, &self_loop);
  std::cout << std::endl;
  tt.toc();
  /* get the number of elements in in abstraction_d1 */
  std::cout << std::endl << "Number of elements in abstraction_d1: " << abstraction_d1.getSize() << std::endl;
    
  /* save transition relation to file */
  scots::SymbolicSet a_d1 = abstraction_d1.getTransitionRelation();
  a_d1.writeToFile("abstraction_d1.bdd");
  std::cout << std::endl << "Abstraction d1 space details:" << std::endl;
  a_d1.printInfo(1);

  /****************************************************************************/
  /* we continue with the controller synthesis */
  /****************************************************************************/

  /* we setup a fixed point object to compute reachabilty controller */
  scots::FixedPoint fp(&abstraction_d1);
  /* the fixed point algorithm operates on the BDD directly */
  tt.tic();
  BDD C = fp.parity(c0.getSymbolicSet(), c1.getSymbolicSet(), c2.getSymbolicSet(), c3.getSymbolicSet());
  tt.toc();

  /****************************************************************************/
  /* last we store the controller as a SymbolicSet 
   * the underlying uniform grid is given by the Cartesian product of 
   * the uniform gird of the space and uniform gird of the input space */
  /****************************************************************************/
  scots::SymbolicSet controller(ss,is);
  controller.setSymbolicSet(C);
  controller.writeToFile("unicycle_controller.bdd");

  return 1;
}